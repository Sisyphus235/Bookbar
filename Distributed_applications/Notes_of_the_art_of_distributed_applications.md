# Preface  
Sun Microsystem released 1st Remote Procedure Call(RPC) Library in 1985.  
XDR--eXternal Data Representation, representing RPC arguments and results in a machine-independent format  

# Chapter 1. Introduction and overview  
"A computer user should be able to access resources on the network **without explicitly requesting** the network transactions required to use the resource."  
"Systems that provide transparent access to resources on the network are referred to as **distributed systems**."  
"A **platform** is a set of routines, typically located in a library, that provide specific functions on which to build your application. "  
"The Open Network Computing (ONC) platform developed by Sun Microsystems addresses the above requirements. The platform consists of the Remote Procedure Call (RPC) routines and the eXternal Data Representation (XDR) routines. "  
Process descriptions:  
1.Local Application -Parameters-> 2.Remote Procedure -Results-> 3.Local Application  
RPC may be on the same computers, and it only requires to make a call to a procedure that does not reside in the address space of the calling process.  
RPC arguments and result may be different between local and remote computer architectures. Therefore, XDR is used to solve the differences.  

