* pytest integrates easily with other tools like continuous integration and web automation. 
* pytest is useful for unit testing, integration testing, system testing(end-to-end testing), functional testing.
    * unit test: check a small bit of code, like a function or a class, in isolation of the rest of the system;
    * integration test: check a larger bit of the code, maybe several classes, or a subsystem;
    * system test(end-to-end): check all of the system under test in an environment as close to the end-user environment as possible;
    * functional test: check a single bit of functionality of a system;
    * subcutaneous test: a test that doesn’t run against the final end-user interface, but against an interface just below the surface. (like API layer)

## Chapter 1. Getting Started with Pytest
* run a pytest:

```python
pytest testxx.py  # 显示简略信息，每通过一个测试，多一个"."
pytest -v testxx.py  #显示详细信息，显示通过什么样的测试
```

* pytest looks for files starting with "test_" or ending with "_test".
naming conventions:
1.Test files should be named test_<sth.>.py or <sth.>_test.py;
2.Test methods and functions should be named test_<sth.>
3.Test classes should be named Test<Sth.>
It can be changed by setting configurations. 
* skip a test:
@pytest.mark.skip()
* markers可以跨文件执行一批测试:
@pytest.marker.<marker_name>
pytest -v -m <marker_name>
* print代码中内容：
-s，例如：pytest -v -s test_1.py

## Chapter 2. Writing Test Functions
* single test
add :: and the test function name

```python
pytest -v x/x/test.py::test_a
```

## Chapter 3. Pytest Fixtures
fixtures are functions that are run by pytest before the actual test functions. 
conftest.py is used to share fixtures among multiple test files.  
fixtures include an optional parameter called scope, which controls how often a fixture gets set up and torn down. The scope parameter to @pytest.fixture() can have the values of function, class, module, or session. The default scope is function.

## Chapter 4. Builtin Fixtures
**tmpdir and  tmpdir_factory** are used to create a temporary file system directory before your test runs, and remove the directory when your test is finished. 

```python
def test_tmpdir(tmpdir):
    # tmpdir has a path name associated with it
    
    # join() extends the path to include a filename
    a_file = tmpdir.join("something.txt")

    # create directories
    a_sub_dir = tmpdir.mkdir('anything')
    
    # create files in user-defined directories
    another_file = a_sub_dir.join("something_else.txt")
    
    # write something to file
    a_file.write("content tested")
    
    # read the filed written just now
    assert a_file.read() == "content tested"
```

these two builtin fixtures are on function scope, which means it cannot stay longer than one test function.

```python
def test_tmpdir_factory(tmpdir_factory):
    # start with making a directory
    a_dir = tmpdir_factory.mktemp('mydir')
    
    # base_temp is the parent of 'mydir'
    base_temp = tmpdir_facotry.getbasetemp()
    print(f'base_temp: {base_temp}')
    
    # the code below is like tmpdir
    a_file = a_dir.join("something.txt")
    a_sub_dir = a_dir.mkdir('anything')
    another_file = a_sub_dir.join('something_else.txt')
    
    a_file.write('content tested')
    assert a_file.read() == 'content tested'
```

tmpdir_factory allows us to use other scopes, such as module scope.

```python
import pytest
import json

@pytest.fixture(scope='module')
def author_file_json(tmpdir_factory):
    """
    write some authors to a data file.
    """
    python_author_data = {
        'Ned': {'City': 'Boston'},
        'Brian': {'City': 'Portland'},
        'Luciano': {'City': 'Sau Paulo'}
        }
    file = tmpdir_factory.mktemp('data').join('author_file.json')
    print(f'file: {str(file)}')
    
    with file.open('w') as f:
        json.dump(python_author_data, f)
        
    return file
```

```python
# some test using previous data file
def test_brian_in_portland(author_file_json):
    """
    a test that uses previous data file
    """
    with author_file_json.open() as f:
        authors = json.load(f)
        assert authors['Brian']['City'] == 'Portland'
        
def test_all_have_cities(author_file_json):
    with author_file_json.open() as f:
        authors = json.load(f)
        for a in authors:
            assert len(authors[a]['City']) > 0
```

**cache builtin fixture** can be used to pass information between test sessions.

```python
pytest --cache-show # show test sessions in cache
```

**capsys builtin fixture** allows testers to retrieve stdout and stderr from some code.

```python
def test_greeting(capsys):
    greeting('Earthling')
    out, err = capsys.readouterr()
    assert out == "Hi, Earthling\n"
    assert err == ""
```

**monkey patch** is a dynamic modification of a class or module during runtime, it provides the following functions:
* setattr(target, name, value=<notset>, raising=True): Set an attribute
* delattr(target, name=<notset>,raising=True): Delete an attribute
* setitem(dic, name, value): Set a dictionary entry
* delitem(dic, name, raising=True): Delete a dictionary entry
* setenv(name, value, prepend=None): Set an envionmental variable
* delenv(name, raising=True): Delete an environmental variable
* syspath_prepend(path): Prepend path to sys.path, which is Python's list of import locations
* chdir(path): Change the current working directory

raising means whether to raise an exception if the item doesn't exist.

```python
def test_def_prefs_change_home(tmpdir, monkeypatch):
    monkeypatch.setenv('HOME', tmpdir.mkdir('home'))
    cheese.write_default_cheese_preferences()
    expected = cheese._default_prefs
    actual = cheese.read_cheese_preferences()
    assert expected == actual

# 原函数有自己的文件路径，如果用原函数路径测试会复写原文件，通过monkeypattch可以改变运行中的文件，tmpdir创建新的路径，用这样的方法测试。另一种方式是改变环境路径

def test_def_prefs_change_expanduser(tmpdir, monkeypatch):
    fake_home_dir = tmpdir.mkdir('home')
    monkeypatch.setattr(cheese.os,path, 'expanduser', (lambda x: x.replace('~', str(fake_home_dir))))
    cheese.write_default_cheese_preferences()
    expected = cheese._default_prefs
    actual = cheese.read_cheese_preferences()
    assert expected == actual
```

**doctest** is used to activate the doctest in python

```python
def multiply(a, b):
    """
    returns a multiplied by b
    >>> import unnecessary_math as um
    >>> um.multiply(4,3)
    12
    >>> um,multiply('a',3)
    'aaa'
    """
    return a * b
    
pytest -v -doctest-modules --tb=short unnecessary_math.py
```

## Chapter 5. Plugins
check which plugins are acive
```shell
pytest --trace-config
```

## Chapter 6. Configuration
* pytest.ini is the primary pytest configuration file
* conftest.py is a local plugin to allow hook functions and fixtures for the directory where the conftest.py file exists and all subdirectories
* __init__.py allows you to have identical test filenames in multiple test directories.

## Chapter 7. Using pytest with Other Tools
pytest usually comes with other tools
* pdb
    * --tb=[auto/long/short/line/native/no]: Contorls the traceback style
    * -v: display all the test names, passing or failing
    * -l: display local variables alongside the stacktrace
    * -lf: run just the tests that failed last
    * -x: stops the test session with the first failure
    * --pdb: starts an interacitve debugging session at the point of failure
* pytest-cov: code coverage can measure what percentage of the code under test is being tested by a test suite, such as
```shell
py.test --cov=<project_path_name>
py.test --cov=<project_path_name> --cov-report=html
```
* mock
mocker fixture is a convenience interface to unitest.mock

* tox
tox is a command line tool that allows you to run your complete suite of tests in multiple environments

* Jenkins CI
Jenkins is a continuous integration system



